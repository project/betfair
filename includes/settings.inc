<?php

/**
 * @file
 * File containing all administrative settings.
 *
 * @ingroup betfair_api
 * @{
 */

/**
 * Admin settings page callback.
 */
function betfair_api_settings() {
  $form['betfair_cache_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache length'),
    '#description' => t('Length of time to cache event prices for. See the <a href="@url">Betfair API comparison</a> page to work out whether you need to increase this setting or not.', array('@url' => 'http://bdp.betfair.com/index.php?option=com_content&task=view&id=36&Itemid=62')),
    '#default_value' => variable_get('betfair_cache_length', 3),
    '#field_suffix' => t('seconds'),
    '#size' => 5,
  );
  $form['betfair_global_api'] = array(
    '#type' => 'textfield',
    '#title' => t('Global API WSDL location'),
    '#default_value' => variable_get('betfair_global_api', 'https://api.betfair.com/global/v3/BFGlobalService.wsdl'),
    '#required' => TRUE,
  );
  $form['betfair_exchange_api'] = array(
    '#type' => 'textfield',
    '#title' => t('Exchange API WSDL location'),
    '#default_value' => variable_get('betfair_exchange_api', 'https://api.betfair.com/exchange/v5/BFExchangeService.wsdl'),
    '#required' => TRUE,
  );
  $form['betfair_developer_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Developer Username'),
    '#description' => t("The username with which to login to the API for a new session. The username must be at least 6 characters and no longer than 20 characters."),
    '#default_value' => variable_get('betfair_developer_username', ''),
    '#required' => TRUE,
  );
  $form['betfair_developer_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Developer password'),
    '#description' => t("The password with which to login to the API for a new session. The password must be at least 8 characters and no longer than 20 characters."),
    '#default_value' => variable_get('betfair_developer_password', ''),
    '#required' => TRUE,
  );
  $form['betfair_product_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Product ID'),
    '#description' => t('The API product ID with which to login to the API for a new session. If you want to use the Free Access API, use 82. If you are a paying API subscriber, use the Id provided when you signed up. To find out more please visit the <a href="@url">Betfair API product</a> page.', array('@url' => 'http://bdp.betfair.com/index.php?option=com_content&task=view&id=81&Itemid=73')),
    '#default_value' => variable_get('betfair_product_id', 82),
    '#required' => TRUE,
  );
  $form['betfair_vendor_software_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Vendor software id'),
    '#description' => t("(optional) The vendor software ID with which to login to the API for a new session. This is only relevant for software vendors and is provided when software vendors sign up.<br />Use '0' when you're not using this setting."),
    '#default_value' => variable_get('betfair_vendor_software_id', 0),
    '#required' => TRUE,
  );
  $form['betfair_location_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Location id'),
    '#description' => t("(optional) The location ID with which to login for a new session.<br />Use '0' when you're not using this setting."),
    '#default_value' => variable_get('betfair_location_id', 0),
    '#required' => TRUE,
  );
  return system_settings_form($form);
} // betfair_api_settings

/**
 * Validation for admin settings page.
 *
 * @see betfair_api_settings()
 */
function betfair_api_settings_validate(&$form, &$form_state) {
  if (!is_numeric($form_state['values']['betfair_cache_length'])) {
    form_set_error('betfair_cache_length', t('Cache setting can be numeric only.'));
  }
  elseif ($form_state['values']['betfair_cache_length'] < 0) {
    form_set_error('betfair_cache_length', t('Cache setting can be a positive number only.'));
  }
  elseif ($form_state['values']['betfair_cache_length'] == 0) {
    drupal_set_message(t('Cache is OFF but please be aware of the <a href="@url">Betfair terms and conditions</a>. You may be charged if you exceed a certain amount of data requests per second.', array('@url' => 'http://www.betfair.com/aboutUs/Betfair.Charges')), 'error');
  }
} // betfair_api_settings_validate

/**
 * @} End of "ingroup betfair_api".
 */